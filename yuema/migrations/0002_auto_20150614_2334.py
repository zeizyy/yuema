# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('yuema', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='comment',
            name='count',
        ),
        migrations.AddField(
            model_name='comment',
            name='seen',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]

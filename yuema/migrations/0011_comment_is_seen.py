# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('yuema', '0010_remove_comment_seen'),
    ]

    operations = [
        migrations.AddField(
            model_name='comment',
            name='is_seen',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]

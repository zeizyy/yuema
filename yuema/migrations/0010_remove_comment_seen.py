# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('yuema', '0009_remove_userprofile_reply'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='comment',
            name='seen',
        ),
    ]

# Yuema README #

Yuema is a Event Planning Web App written in a Python-basesd MVC framework Django. Main functionalities include creating events, sharing events, inviting friends to events, commenting
on events and message alerts. The website also uses Baidu (a Chinese Search Engine) Map API so
that user can see directly on the map the event venue. In addition, technologires like Ajax, jQuery and CSS are utilized for better UX. The live site can be viewed at http://shielded-earth-4593.herokuapp.com/.

More features are under development!
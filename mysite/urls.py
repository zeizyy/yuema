from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from yuema import views


urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'mysite.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^yuema/', include('yuema.urls')),
    url(r'^$',views.index,name='index'),
    url(r'^accounts/profile/', views.index,name='index'),
    url(r'^accounts/login/$', 'django.contrib.auth.views.login'),
)

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('yuema', '0016_expressuser'),
    ]

    operations = [
        migrations.AddField(
            model_name='expressuser',
            name='event',
            field=models.ForeignKey(null=True, to='yuema.Event'),
            preserve_default=True,
        ),
    ]

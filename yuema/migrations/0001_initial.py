# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Comment',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('content', models.CharField(max_length=5000)),
                ('pub_date', models.DateTimeField(default=datetime.datetime.today)),
                ('count', models.IntegerField(default=0)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('name', models.CharField(max_length=200)),
                ('start_time', models.DateTimeField(default=datetime.datetime.today)),
                ('pub_date', models.DateTimeField(default=datetime.datetime.today)),
                ('private', models.BooleanField(default=False)),
                ('location', models.CharField(max_length=1000)),
                ('description', models.CharField(max_length=5000)),
                ('cap', models.IntegerField()),
                ('host', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Request',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('is_seen', models.BooleanField(default=False)),
                ('is_handled', models.BooleanField(default=False)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='UserProfile',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('first_name', models.CharField(max_length=100)),
                ('last_name', models.CharField(max_length=100)),
                ('activation_key', models.CharField(max_length=40, blank=True)),
                ('key_expires', models.DateTimeField(default=datetime.date(2015, 6, 14))),
                ('request_count', models.IntegerField(default=0)),
                ('events', models.ManyToManyField(null=True, blank=True, to='yuema.Event')),
                ('friends_p', models.ManyToManyField(related_name='friends', null=True, blank=True, to='yuema.UserProfile')),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name_plural': 'User profiles',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='request',
            name='acceptor',
            field=models.ForeignKey(related_name='acceptor', to='yuema.UserProfile'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='request',
            name='requestor',
            field=models.ForeignKey(related_name='requestor', to='yuema.UserProfile'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='event',
            name='join_alert',
            field=models.ManyToManyField(null=True, blank=True, to='yuema.UserProfile'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='comment',
            name='event',
            field=models.ForeignKey(to='yuema.Event'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='comment',
            name='reply_to',
            field=models.ForeignKey(related_name='reply_to', null=True, blank=True, to='yuema.UserProfile'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='comment',
            name='writer',
            field=models.ForeignKey(related_name='writer', null=True, to='yuema.UserProfile'),
            preserve_default=True,
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('yuema', '0014_auto_20150618_0935'),
    ]

    operations = [
        migrations.CreateModel(
            name='EventInvite',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('is_seen', models.BooleanField(default=False)),
                ('event', models.ForeignKey(to='yuema.Event')),
                ('invitee', models.ForeignKey(to='yuema.UserProfile')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('yuema', '0013_auto_20150616_1418'),
    ]

    operations = [
        migrations.CreateModel(
            name='JoinAlert',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('is_seen', models.BooleanField(default=False)),
                ('event', models.ForeignKey(to='yuema.Event')),
                ('user_profile', models.ForeignKey(to='yuema.UserProfile')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.RemoveField(
            model_name='event',
            name='join_alert',
        ),
    ]

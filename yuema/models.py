#coding=utf-8 
from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
import datetime

# Create your models here.

class UserProfile(models.Model):
	user = models.OneToOneField(User)
	events = models.ManyToManyField('Event', null=True,blank=True)
	first_name = models.CharField(max_length=100)
	last_name = models.CharField(max_length=100)
	activation_key = models.CharField(max_length=40, blank=True)
	key_expires = models.DateTimeField(default=datetime.datetime.today)
	friends_p = models.ManyToManyField('UserProfile',related_name='friends',null=True,blank=True)
	# requests_p = models.ManyToManyField('UserProfile',related_name='friend_request',null=True,blank=True)
	request_count = models.IntegerField(default=0)

	# def __unicode__(self):
	def __str__(self):
		return self.last_name + " "+ self.first_name+" ("+self.user.username+")"

	def full_name(self):
		return self.last_name + " "+ self.first_name

	def count_inc(self):
		self.request_count = self.request_count + 1
		self.save()

	def count_dec(self):
		self.request_count = self.request_count - 1
		self.save()

	def zero(self):
		self.request_count = 0
		self.save()

	class Meta:
		verbose_name_plural=u'User profiles'


class ExpressUser(models.Model):
	name = models.CharField(max_length=254)
	event = models.ForeignKey('Event',null=True)
	# def __unicode__(self):
	def __str__(self):
		return self.name

# class Participate(models.Model):
# 	event = models.OneToOneField(Event)
# 	user = models.OneToOneField(User)

# 	def __str__(self):
# 		return self.user.username + " participates in "+ self.event.name

class Request(models.Model):
	requestor = models.ForeignKey(UserProfile,related_name="requestor")
	acceptor = models.ForeignKey(UserProfile,related_name="acceptor")
	is_seen = models.BooleanField(default=False)
	is_handled = models.BooleanField(default=False)

	def handle(self):
		self.is_handled = True
		self.save()

	# def __unicode__(self):
	def __str__(self):
		if self.is_seen:
			s="seen"
		else:
			s="not seen"
		return "Request sent from "+self.requestor.full_name()+" to "+self.acceptor.full_name()+" ("+s+")"


class Event(models.Model):
	name = models.CharField(max_length=200)
	host = models.ForeignKey(UserProfile,related_name="host",null=True,blank=True)
	start_time = models.DateTimeField(default=datetime.datetime.today)
	pub_date = models.DateTimeField(default=datetime.datetime.today)
	private = models.BooleanField(default=False)
	location = models.CharField(max_length=1000)
	description = models.CharField(max_length=5000)
	cap = models.IntegerField()

	# def __unicode__(self):
	def __str__(self):
		return self.name + " by "+self.host.last_name+" "+self.host.first_name

	def has_happened(self):
		now = timezone.now()
		return now >= self.start_time

class JoinAlert(models.Model):
	user_profile = models.ForeignKey(UserProfile)
	event = models.ForeignKey(Event)
	is_seen = models.BooleanField(default=False)

	# def __unicode__(self):
	def __str__(self):
		if self.is_seen:
			flag = "Seen"
		else:
			flag = "Not Seen"
		return self.user_profile.full_name() + " joins " + str(self.event) +" ("+flag+")"

	def see(self):
		self.is_seen=True
		self.save()

class EventInvite(models.Model):
	invitee = models.ForeignKey(UserProfile)
	event = models.ForeignKey(Event)
	is_seen = models.BooleanField(default=False)
	# def __unicode__(self):
	def __str__(self):
		if self.is_seen:
			flag = "Seen"
		else:
			flag = "Not Seen"
		return self.event.host.full_name() + " invites "+self.invitee.full_name() + " to 【"+self.event.name+"】"

class CommentAlert(models.Model):
	user_profile = models.ForeignKey(UserProfile)
	comment = models.ForeignKey('Comment')
	is_seen = models.BooleanField(default=False)

	# def __unicode__(self):
	def __str__(self):
		if self.is_seen:
			flag = "Seen"
		else:
			flag = "Not Seen"
		return self.comment.writer.full_name() + " replies " + self.user_profile.full_name() + ": "+self.comment.content

	def see(self):
		self.is_seen=True
		self.save()

	def message(self):
		return self.comment.writer.full_name()

class Comment(models.Model):
	writer = models.ForeignKey(UserProfile,related_name='writer',null=True)
	# reply_to = models.ManyToManyField(UserProfile,related_name='reply_to',null=True,blank=True)
	event = models.ForeignKey(Event)
	content = models.CharField(max_length=5000)
	pub_date = models.DateTimeField(default=datetime.datetime.today)
	is_seen = models.BooleanField(default=False)

	def __str__(self):
	# def __unicode__(self):
		return self.content

	def get_reply_to(self):
		#get users the comment replies to
		reply_to = CommentAlert.objects.filter(comment = self)
		count = len(reply_to)
		if count == 1:
			return reply_to.first().user_profile
		else:
			return None

	def time_diff(self):
		diff = timezone.now()-self.pub_date
		diff_in_sec = diff.total_seconds()
		if diff_in_sec < 24*3600:
			if diff_in_sec < 60:
				return str(int(diff_in_sec)) + " seconds ago"
			elif diff_in_sec <3600:
				return str(int(diff_in_sec/60)) + " minutes ago"
			else:
				return str(int(diff_in_sec/3600)) + " hours ago"
		else:
			return self.pub_date

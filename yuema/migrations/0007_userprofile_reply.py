# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('yuema', '0006_remove_comment_reply_to'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='reply',
            field=models.ManyToManyField(blank=True, null=True, to='yuema.Comment'),
            preserve_default=True,
        ),
    ]

#coding=utf-8 
from django.shortcuts import render, render_to_response
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.core.urlresolvers import reverse
from django.template import RequestContext
from yuema.forms import EventForm,LoginForm, UserForm
from yuema.models import Event,UserProfile, Request, Comment, CommentAlert, JoinAlert,EventInvite, ExpressUser
from django.template.response import TemplateResponse

# authenticate
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.contrib.auth import login as auth_login
from django.contrib.auth.decorators import login_required

#csrf exempt
from django.views.decorators.csrf import csrf_exempt

#register
import random,hashlib,datetime
from django.core.mail import send_mail
# Create your views here.
# @login_required
def index(request):
	if request.user.is_authenticated():
		return HttpResponseRedirect('/yuema/accounts/profile/')
	else:
		return HttpResponseRedirect('/yuema/accounts/login/')

def profile1(request):
	return render_to_response('account/profile1.html')

@login_required
def profile(request):
	context = RequestContext(request)
	user = request.user
	user_p = UserProfile.objects.get(user=user)
	#All events host by friends
	# events = Event.objects.order_by('-pub_date')

	events = []
	friends = user_p.friends.all()
	for f in friends:
		e = Event.objects.filter(host=f).filter(private=False)
		events.extend(e)

	# events = list(set(events))

	events_p = user_p.events.all()
	#Past events
	events_p_p =[]
	#incoming events
	events_p_i =[]
	for e in events_p:
		if e.has_happened():
			events_p_p.append(e)
		else:
			events_p_i.append(e)
	#event the user hosted
	event_h = Event.objects.filter(host=user_p)
	
	#createEvent form
	if request.method == 'POST':
		event_form = EventForm(request.POST)
		if event_form.is_valid():
			event_form = EventForm(data=request.POST)
			event = event_form.save()
			user_p.events.add(event)
			#add goers to the event
			# goer = request.POST.getlist('goer')
			# for g in goer:
			# 	user = User.objects.get(pk=g)
			# 	user_p = UserProfile.objects.get(user=user)
			# 	user_p.events.add(event)
			# user_p.events.add(event_form)
			# Redirect to the document list after POST
			return render_to_response('yuema/test.html')
			return HttpResponseRedirect('/yuema/accounts/profile/')
	else:
		event_form = EventForm(initial={'host':user_p}) # A empty, unbound form
	return render_to_response('account/profile.html',{'event_form':event_form,'user':user,'user_p':user_p,'events':events,'events_p_p':events_p_p,'events_p_i':events_p_i,'event_h':event_h},context)

@login_required
def accounts(request):
	current_user = request.user
	current_user_p = UserProfile.objects.get(user=current_user)
	requests = Request.objects.filter(acceptor=current_user_p).filter(is_handled=False)
	requests_handled = Request.objects.filter(acceptor=current_user_p).filter(is_handled=True)
	friends = current_user_p.friends_p.all()
	current_user_p.zero()
	#get all the unseen event alerts
	event_alerts = JoinAlert.objects.filter(event__host=current_user_p).filter(is_seen=False)
	#see all the alerts
	for e_a in event_alerts:
		e_a.see()
	# return HttpResponse(event_alerts)
	c_a = CommentAlert.objects.filter(user_profile=current_user_p)
	comments = []
	for c in c_a:
		comments.append(c.comment)
	c_a_unseen = c_a.filter(is_seen=False)
	for c in c_a_unseen:
		c.is_seen = True
		c.save()
	# event invites
	event_invites = EventInvite.objects.filter(invitee=current_user_p)
	# return render_to_response('yuema/test.html')
	return render_to_response('account/accounts.html',{'event_invites':event_invites,'c_a_unseen':c_a_unseen,'comments':comments,'event_alerts':event_alerts,'user_p':current_user_p,'requests':requests,'requests_handled':requests_handled,'friends':friends})

@login_required
def createEvent(request):
	context = RequestContext(request)
	user = request.user
	user_p = UserProfile.objects.get(user=user)
	if request.method == 'POST':
		event_form = EventForm(request.POST)
		if event_form.is_valid():
			event_form = EventForm(data=request.POST)
			event = event_form.save()
			user_p.events.add(event)
			#add goers to the event
			# goer = request.POST.getlist('goer')
			# for g in goer:
			# 	user = User.objects.get(pk=g)
			# 	user_p = UserProfile.objects.get(user=user)
			# 	user_p.events.add(event)
			# user_p.events.add(event_form)
	#       # Redirect to the document list after POST
			return HttpResponseRedirect('/yuema/accounts/profile/')
	else:
		event_form = EventForm(initial={'host':user_p}) # A empty, unbound form
	return render_to_response('yuema/createEvent.html',{'user_p':user_p,'event_form':event_form},context)

@login_required
def viewEvent(request,event_id):
	user = request.user
	user_p = UserProfile.objects.get(user=user)
	event = Event.objects.get(pk=event_id)
	if event.private and not user_p in event.host.friends_p.all() and not user_p == event.host:
		return HttpResponseRedirect(reverse('yuema.views.viewUser',args=[event.host.id]))
	host = event.host
	#get all friends of the host
	friends = host.friends_p.all()
	start_time = event.start_time
	goers = event.userprofile_set.all()
	friends_r = []
	for f in friends:
		if f not in goers:
			try:
				EventInvite.objects.get(event=event,invitee=f)
			except:
				friends_r.append(f)
	goers_e = event.expressuser_set.all()
	# return HttpResponse(goers_e)
	location = event.location
	description = event.description
	cap = event.cap
	count = len(goers)+len(goers_e)
	# user already join the event true=quit
	flag_join = user_p in goers
	# the event is full
	flag_full = count >= int(cap)
	comments = Comment.objects.filter(event=event)
	#get all the commenters
	writer = []
	for c in comments:
		if not c.writer == user_p:
			writer.append(c.writer)
	writer = list(set(writer))
	return render_to_response('yuema/viewEvent.html',{'friends_r':friends_r,'goers_e':goers_e,'friends':friends,'writer':writer,'event':event, 'start_time':start_time,'goers':goers,'location':location,'description':description,'cap':cap,'host':host,'count':count, 'user':user,'flag_join':flag_join,'flag_full':flag_full,'user_p':user_p,'comments':comments})

def shareEvent(request,event_id):
	event = Event.objects.get(pk=event_id)
	host = event.host
	start_time = event.start_time
	goers = event.userprofile_set.all()
	goers_e = event.expressuser_set.all()
	location = event.location
	description = event.description
	cap = event.cap
	count = len(goers)
	flag = request.user.is_authenticated()
	# if flag:
	# 	return HttpResponseRedirect(reverse('yuema.views.viewEvent',args=[event_id]))
	return render_to_response('yuema/shareEvent.html',{'goers_e':goers_e,'share':True,'flag':flag,'event':event,'host':host,'start_time':start_time,'goers':goers,'location':location,'description':description,'cap':cap,'count':count})

@login_required
@csrf_exempt
def inviteEvent(request):
	friends = request.POST.getlist('friends')
	event_id = request.POST['event_id']
	event = Event.objects.get(pk=event_id)
	for f in friends:
		invitee = UserProfile.objects.get(pk=f)
		try:
			EventInvite.objects.get(invitee=invitee,event=event)
		except:
			event_invite = EventInvite(invitee=invitee,event=event)
			event_invite.save()
		invitee.count_inc()
	return HttpResponseRedirect(reverse('yuema.views.viewEvent',args=[event_id]))

@login_required
def acceptInvite(request, invite_id):
	e_i = EventInvite.objects.get(pk=invite_id)
	e_i.invitee.events.add(e_i.event)
	e_i.delete()
	return HttpResponseRedirect(reverse('yuema.views.accounts'))

@login_required
def declineInvite(request, invite_id):
	e_i = EventInvite.objects.get(pk=invite_id)
	e_i.delete()
	return HttpResponseRedirect(reverse('yuema.views.accounts'))

@csrf_exempt
def join(request):
	context = RequestContext(request)
	user_p = UserProfile.objects.get(user=request.user)
	event_id = request.POST['event_id']
	flag = request.POST['flag']
	event = Event.objects.get(pk=event_id)
	if flag=="True":
		user_p.events.remove(event)
		try:
			j = JoinAlert.objects.get(user_profile=user_p,event=event)
			j.delete()
			event.join_alert.remove(user_p)
			event.host.count_dec()
		except:
			pass
	else:
		user_p.events.add(event)
		try:
			j = JoinAlert.objects.get(user_profile=user_p,event=event)
		except:
			j_a = JoinAlert(user_profile=user_p,event=event)
			j_a.save()
			event.host.count_inc()
	return HttpResponseRedirect(reverse('yuema.views.viewEvent',args=[event_id]), context)

@csrf_exempt
def expressJoin(request):
	name = request.POST['name']
	if not name:
		return HttpResponse("NULL!")
	event_id = request.POST['event_id']
	event = Event.objects.get(pk=event_id)
	e_u = ExpressUser(name=name, event=event)
	e_u.save()
	return HttpResponseRedirect(reverse('yuema.views.shareEvent',args=[event_id]))


@login_required
def viewUser(request,user_id):
	#current user
	user_p = UserProfile.objects.get(user=request.user)
	#user looked at
	u_p = UserProfile.objects.get(pk=user_id)
	events = u_p.events.all().filter(private=False)
	is_current = user_p == u_p
	is_friend = user_p in u_p.friends_p.all()
	r = Request.objects.filter(acceptor=u_p).filter(requestor=user_p)
	return render_to_response('yuema/viewUser.html',{'u_p':u_p,'user_p':user_p,'events':events,'is_current':is_current,'is_friend':is_friend,'r':r})

@login_required
def addUser(request,user_id):
	current_user_p = UserProfile.objects.get(user=request.user)
	user_p = UserProfile.objects.get(pk=user_id)
	#test whether the request is sent
	r_test = Request.objects.filter(requestor=current_user_p).filter(acceptor=user_p)
	if r_test:
		return render_to_response('yuema/error.html')
	else:
		try:
			Request.objects.get(requestor=current_user_p,acceptor=user_p)
		except:
			r = Request(requestor=current_user_p,acceptor=user_p)
			r.save()
		user_p.count_inc()
		return HttpResponseRedirect(reverse('yuema.views.viewUser',args=[user_id]))

@login_required
def removeUser(request, user_id):
	user_p = UserProfile.objects.get(user=request.user)
	friend_p = UserProfile.objects.get(pk=user_id)
	user_p.friends_p.remove(friend_p)
	friend_p.friends_p.remove(user_p)
	try:
		r = Request.objects.get(requestor=user_p,acceptor=friend_p)
		r.delete()
	except:
		pass
	try:
		r = Request.objects.get(requestor=friend_p,acceptor=user_p)
		r.delete()
	except:
		pass
	return HttpResponseRedirect(reverse('yuema.views.viewUser',args=[user_id]))

@csrf_exempt
@login_required
def deleteGoer(request):
	event_id = request.POST['event_id']
	goer_id = request.POST['goer_id']
	event = Event.objects.get(pk=event_id)
	goer = UserProfile.objects.get(pk=goer_id)
	goer.events.remove(event)
	return HttpResponseRedirect(reverse('yuema.views.viewEvent',args=[event_id]))

@csrf_exempt
@login_required
def deleteExpress(request):
	event_id = request.POST['event_id']
	goer_id = request.POST['goer_id']
	event = Event.objects.get(pk=event_id)
	goer = ExpressUser.objects.get(pk=goer_id)
	goer.delete()
	return HttpResponseRedirect(reverse('yuema.views.viewEvent',args=[event_id]))


@login_required
def acceptRequest(request,request_id):
	r = Request.objects.get(pk=request_id)
	r.acceptor.friends_p.add(r.requestor)
	r.requestor.friends_p.add(r.acceptor)
	r.handle()
	r_acceptor = Request.objects.filter(requestor=r.acceptor).filter(acceptor=r.requestor)
	if r_acceptor:
		r_acceptor.first().handle()
	return HttpResponseRedirect('/yuema/accounts')


@login_required
def declineRequest(request,request_id):
	return HttpResponse("Hello!")

@login_required
def editEvent(request,event_id):
	user_p = UserProfile.objects.get(user=request.user)
	try:
		event = Event.objects.get(id=event_id)
	except Event.DoesNotExist:
		raise Http404("活动不存在！")
	if not event.host == user_p:
		return render_to_response('yuema/error.html')
	context = RequestContext(request)
	if request.POST:
		event_form = EventForm(request.POST,instance=event)
		if event_form.is_valid():
			event_form.save()
			return HttpResponseRedirect(reverse('yuema.views.viewEvent',args=[event.id]), context)
	else:
		event_form = EventForm(instance=event)
	return render_to_response('yuema/editEvent.html',{'event_form':event_form,'event':event,'user_p':user_p},context)

@login_required
def deleteEvent(request,event_id):
	user_p = UserProfile.objects.get(user=request.user)
	try:
		event = Event.objects.get(id=event_id)
	except Event.DoesNotExist:
		raise Http404("Event does not exist")
	if not event.host == user_p:
		return render_to_response('yuema/error.html')
	event.delete()
	return HttpResponseRedirect('/')


@login_required
@csrf_exempt
def searchUser(request):
	context = RequestContext(request)
	user_p = UserProfile.objects.get(user=request.user)
	found=True
	if request.POST:
		username = request.POST['username']
		try:
			user = User.objects.get(username=username)
		except User.DoesNotExist:
			found = False
			return render_to_response('yuema/searchUser.html',{'found':found,'user_p':user_p})
		user_p = UserProfile.objects.get(user=user)
		return HttpResponseRedirect(reverse('yuema.views.viewUser',args=[user_p.id]))
	else:
		return render_to_response('yuema/searchUser.html',{'found':found,'user_p':user_p}, context)

@login_required
@csrf_exempt
def addComment(request):
	content = request.POST['comment']
	event_id = request.POST['event_id']
	reply_to = request.POST['reply_to']
	# if not content:
	# 	empty=True
	# 	return HttpResponseRedirect(reverse('yuema.views.viewEvent',args=[event_id]))
	user_p = UserProfile.objects.get(user=request.user)
	event = Event.objects.get(pk=event_id)
	if reply_to == "":
		#get all the commenters
		comments = Comment.objects.filter(event=event)
		r = []
		for c in comments:
			if not c.writer == user_p:
				r.append(c.writer)
		r = list(set(r))
	else:
		r = [UserProfile.objects.get(pk=reply_to)]

	c = Comment(writer=user_p,event=event,content=content)
	c.save()

	for w in r:
		c_a = CommentAlert(user_profile=w,comment=c)
		c_a.save()
		#increase alerts of users in r by 1
		w.count_inc()
	return HttpResponseRedirect(reverse('yuema.views.viewEvent',args=[event_id]))

@login_required
@csrf_exempt
def replyComment(request,comment_id):
	return HttpResponse('Hello')

@login_required
@csrf_exempt
def deleteComment(request, comment_id):
	event_id=request.POST['event_id']
	c = Comment.objects.get(pk=comment_id)
	c_a = c.commentalert_set.all()
	for alert in c_a:
		alert.delete()
	c.delete()
	return HttpResponseRedirect(reverse('yuema.views.viewEvent',args=[event_id]))

# def login(request, template_name='registration/login.html'):
# 	context = RequestContext(request)
# 	if request.method == "POST":
# 		form = LoginForm(request, data=request.POST)
# 		if form.is_valid():

# 			# Ensure the user-originating redirection url is safe.
# 			# Okay, security check complete. Log the user in.
# 			username = request.POST['username']
# 			password = request.POST['password']
# 			user = authenticate(username=username, password=password)
# 			if user is not None:
# 				if user.is_active:
# 					auth_login(request, user)
# 					return HttpResponseRedirect('/yuema/')
# 				else:
# 					return HttpResponse('Login unsuccessful. You can still run encrypt and decrypt though.')
# 			else:
# 				return HttpResponse('Login unsuccessful. You can still run encrypt and decrypt though.')
# 	else:
# 		login_form = LoginForm()
# 		return TemplateResponse(request, template_name, context)

	#         return HttpResponseRedirect(redirect_to)
	# else:
	#     form = authentication_form(request)

	# current_site = get_current_site(request)

	# context = {
	#     'form': form,
	#     redirect_field_name: redirect_to,
	#     'site': current_site,
	#     'site_name': current_site.name,
	# }
	# if extra_context is not None:
	#     context.update(extra_context)
	# return TemplateResponse(request, template_name, context,
	#                         current_app=current_app)





def login(request):
	context = RequestContext(request)

	if request.method == 'POST':
		
		login_form = LoginForm(data=request.POST)
		if login_form.is_valid():
			username = request.POST['username']
			password = request.POST['password']

			user = authenticate(username=username, password=password)
			return HttpResponse(user.username)
			#print(user)
			if user is not None:
					if user.is_active:
						auth_login(request, user)
						return HttpResponseRedirect('/yuema/')
					else:
						return HttpResponse('Login unsuccessful. You can still run encrypt and decrypt though.')
			else:
				return HttpResponse('Login unsuccessful. You can still run encrypt and decrypt though.')
	else:
		login_form = LoginForm()
		return render_to_response('account/login.html', context)

# def logout(request):
# 	

def register(request):
	# Like before, get the request's context.
	context = RequestContext(request)

	# A boolean value for telling the template whether the registration was successful.
	# Set to False initially. Code changes value to True when registration succeeds.
	registered = False

	# If it's a HTTP POST, we're interested in processing form data.
	if request.method == 'POST':
		# Attempt to grab information from the raw form information.
		# Note that we make use of both UserForm and UserProfileForm.
		user_form = UserForm(data=request.POST)
		
		try:
			next_page = request.GET['next']
		except:
			next_page='/'

		# If the two forms are valid...
		if user_form.is_valid():
			# Save the user's form data to the database.
			user = user_form.save()	

			# Now we hash the password with the set_password method.
			# Once hashed, we can update the user object.
			user.set_password(user.password)
			user.is_active = True
			user.save()

			# username = user_form.cleaned_data['username']
			# email = user_form.cleaned_data['email']
			# random_string = str(random.random()).encode('utf8')
			# salt = hashlib.sha1(random_string).hexdigest()[:5]
			# salted = (salt + email).encode('utf8')
			# activation_key = hashlib.sha1(salted).hexdigest()
			# key_expires = datetime.datetime.today() + datetime.timedelta(2)

			# Create and save user profile                                                                                                                                  
			# new_profile = UserProfile(user=user, activation_key=activation_key, 
			# 	key_expires=key_expires)
			first_name = request.POST['first']
			last_name = request.POST['last']
			username=request.POST['username']
			password=request.POST['password']
			new_profile = UserProfile(user=user)
			new_profile.first_name = first_name
			new_profile.last_name = last_name
			new_profile.save()
			user = authenticate(username=username, password=password)
			auth_login(request, user)
			
			# Send email with activation key
			# email_subject = 'Account confirmation'
			# email_body = "Hey %s, thanks for signing up. To activate your account, click this link within \
			# 48hours http://ancient-beach-5770.herokuapp.com/SecureWitness/confirm/%s" % (username, activation_key)

			# send_mail(email_subject, email_body, 'event_organizer@163.com',
			# 	[email], fail_silently=False)
			# Now sort out the UserProfile instance.
			# Since we need to set the user attribute ourselves, we set commit=False.
			# This delays saving the model until we're ready to avoid integrity problems.

			#get next from getlist
			# next_page = request.GET
			# return HttpResponse(next_page)

			# Update our variable to tell the template registration was successful.
			registered = True
			return HttpResponseRedirect(next_page)
		# Invalid form or forms - mistakes or something else?
		# Print problems to the terminal.
		# They'll also be shown to the user.
		else:
			print(user_form.errors)

	# Not a HTTP POST, so we render our form using two ModelForm instances.
	# These forms will be blank, ready for user input.
	else:
		user_form = UserForm()
		try:
			next_page = request.GET['next']
		except:
			next_page=""

	# Render the template depending on the context.
	return render_to_response(
			'registration/register.html',
			{'user_form': user_form, 'registered': registered,'next_page':next_page},
			context)

def map(request):
	return render_to_response('yuema/map.html')

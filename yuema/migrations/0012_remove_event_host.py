# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('yuema', '0011_comment_is_seen'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='event',
            name='host',
        ),
    ]

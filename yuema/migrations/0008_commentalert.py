# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('yuema', '0007_userprofile_reply'),
    ]

    operations = [
        migrations.CreateModel(
            name='CommentAlert',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, serialize=False, verbose_name='ID')),
                ('is_seen', models.BooleanField(default=False)),
                ('comment', models.ForeignKey(to='yuema.Comment')),
                ('user_profile', models.ForeignKey(to='yuema.UserProfile')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]

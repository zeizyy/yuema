#coding=utf-8 
from django.contrib.auth.models import User, Group, Permission
from django import forms
from yuema.models import Event
from django.forms import EmailInput,Textarea, NumberInput, DateTimeInput, ModelMultipleChoiceField,CheckboxInput, TextInput, PasswordInput


class LoginForm(forms.Form):
	username = forms.CharField(label = 'Username: ')
	password = forms.CharField(label = 'Password: ', widget = forms.PasswordInput(attrs={'class':'form-control'}))

class EventForm(forms.ModelForm):
	# goer = ModelMultipleChoiceField(queryset=User.objects.all())
	class Meta:
		model = Event
		fields = ('name','start_time','location','description','cap','private','host')
		labels = {
			'name':('活动名称'),
			'start_time':('开始时间'),
			'location':('活动地点'),
			'description':('活动描述'),
			'cap':('人数上限'),
			'private':('设为私密'),
		}
		help_texts = {
			'private':('活动不会出现在好友的Dashboard上'),
		}
		widgets = {
			'name': TextInput(attrs={'class':'form-control'}),
			# 'start_time': DateTimeInput(attrs={'id':'time_picker','class':'form-control'}),
			'start_time': DateTimeInput(attrs={'class':'form-control'}),
			'location': TextInput(attrs={'class':'form-control'}),
            'description': Textarea(attrs={'class':'form-control'}),
            'cap': NumberInput(attrs={'class':'form-control'}),
            # 'private': Select(attrs={'class':'form-control'}),
            'host':forms.HiddenInput(),
            # 'goer':CheckboxInput()
            # 'start_time': 
        }

class UserForm(forms.ModelForm):
	password = forms.CharField(widget=forms.PasswordInput(attrs={'class':'form-control'}), label='密码')
	last = forms.CharField(max_length=30, label='姓',widget=forms.TextInput(attrs={'class':'form-control'}))
	first = forms.CharField(max_length=30, label='名',widget=forms.TextInput(attrs={'class':'form-control'}))

	class Meta:
		model = User
		fields = ('username', 'email', 'password','last','first')
		labels = {
			'email':('电子邮箱'),
			'username':('用户名')
		}
		widgets = {
			'email': EmailInput(attrs={'class':'form-control'}),
			'username': TextInput(attrs={'class':'form-control'}),
		}
		help_texts = {
			'username':('')
		}

	def clean_email(self):
		email = self.cleaned_data["email"]
		try:
			User._default_manager.get(email=email)
		except User.DoesNotExist:
			return email
		raise forms.ValidationError('Email重复！')

	#modify save() method so that we can set user.is_active to False when we first create our user
	def save(self, commit=True):        
		user = super(UserForm, self).save(commit=False)
		user.email = self.cleaned_data['email']
		if commit:
			user.is_active = False # not active until he opens activation link
			user.save()

		return user

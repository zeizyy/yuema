# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('yuema', '0004_remove_comment_reply_to'),
    ]

    operations = [
        migrations.AddField(
            model_name='comment',
            name='reply_to',
            field=models.ManyToManyField(null=True, related_name='reply_to', to='yuema.UserProfile', blank=True),
            preserve_default=True,
        ),
    ]

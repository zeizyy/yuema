# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('yuema', '0012_remove_event_host'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='host',
            field=models.ForeignKey(null=True, to='yuema.UserProfile', related_name='host', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='event',
            name='join_alert',
            field=models.ManyToManyField(to='yuema.UserProfile', related_name='join_alert', null=True, blank=True),
            preserve_default=True,
        ),
    ]

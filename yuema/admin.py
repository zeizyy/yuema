from django.contrib import admin
from yuema.models import Event, UserProfile, Request, Comment,CommentAlert, JoinAlert,EventInvite, ExpressUser
from django.contrib.auth.models import User
from import_export import resources
from import_export.admin import ImportExportModelAdmin, ImportExportMixin


# Register your models here.
admin.site.register(Request)
admin.site.register(Comment)
admin.site.register(CommentAlert)
admin.site.register(JoinAlert)
admin.site.register(EventInvite)
admin.site.register(ExpressUser)


#Event
class EventResource(resources.ModelResource):
    class Meta:
        model = Event

class EventAdmin(ImportExportModelAdmin):
    resource_class = EventResource
    pass

# Register your models here.
admin.site.register(Event,EventAdmin)

#UserProfile
class UserProfileResource(resources.ModelResource):
    class Meta:
        model = UserProfile

class UserProfileAdmin(ImportExportModelAdmin):
    resource_class = UserProfileResource
    pass

# Register your models here.
admin.site.register(UserProfile,UserProfileAdmin)

#User
# class UserResource(resources.ModelResource):
#     class Meta:
#         model = User

# class UserAdmin(ImportExportModelAdmin):
#     resource_class = UserResource
#     pass

# # Register your models here.
# admin.site.register(User,UserAdmin)
